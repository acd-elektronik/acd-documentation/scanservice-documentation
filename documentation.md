### ACD ScanService Documentation  
### Documentation on how to use the service  
### Version: 1.00

![](images/icon.png)

**Content**

1. ACD ScanService  
    1.1. Reception of bar codes  
    1.2. Keyboard looping in  
    1.3. Sending a broadcast  
    1.4. Use of the messenger  
    1.5. Using the Messenger for the M2Longrange Module  
    1.6. Examples of Connecting  
2. The API of the service  
    2.1. Message types  
    2.2. Barcode types  
3. Scanner configuration  

## ACD ScanService
The ACD ScanService offers access to the scanner in order to configure it and forward scanned bar codes. It also intercepts the scanner keys and can trigger the scanner. Scanned bar codes are received by the ACD ScanService and forwarded depending on configuration.

If the default behavior is not changed when reading a bar code, then the read bar code is reported to the system as a keyboard event.

This can be used in order to scan data into generic applications that cannot be adapted for communication with the ACD ScanService. In this case, the app notices no difference between a scan and the use of a keyboard.

For self-developed applications, it is recommended that you bind these to the ACD ScanService. This way, Android will recognize that the ACD ScanService is required for the application and keep it active.

## Reception of bar codes
There are several possibilities for delivering a bar code from the service to the target application. These can be set in the default settings or set by an app via the API of the service.

(See SET_TARGET).

### Keyboard looping in
Here, the service loops the bar code into the Android keyboard system. This can sooner be compared with an external keyboard, however not with the concept of the virtual keyboard.

This mechanism can enter data in each active input field. Thus, for example, a WLAN password can be scanned into the input field instead of typed.


However, the looping in has its limits. The underlying Android system program can only handle a basic character set, so that in addition to the letters A - z (upper- and lower-case) and the numbers 0 - 9, hardly any other characters are accepted. The behavior depends on existing keyboard layouts in the system and can thus not be guaranteed for all conceivable bar code characters.

### Sending a broadcast
The service sends a broadcast intent with the name `de.acdgruppe.scanservice.SCANBROADCAST`.

If the service version is older than 1.22, the broadcast intent name is `de.acdgruppe.scanservice:SCANBROADCAST`. In the “extra” fields `SCANNERDATA_BARCODE` and `SCANNERDATA_BARCODETYPE`, this intent includes the bar code and its type as string. The type of the barcode is transferred as an integer in the `SCANNERDATA_TYPECODE` field.

An app that registers itself as BroadcastReceiver can receive the bar code this way and process according it to its needs. In particular, for fields without optional keyboard input, the screen keyboard can be omitted, which keeps the input window clearer. This method is suitable for application cases where the scanner is configured permanently and can thus provide the app with an uncomplicated reception mechanism. However, it must be noted that any application can receive the broadcast.

### Use of the messenger
An application that binds itself to the service can create a “messenger” via the binding and use it to communicate with the service. If the service is configured for the sending of bar codes via messenger, the data is sent directly to the application that was the last one to register with the service. The binding also ensures that Android does not exit the service during the app’s runtime.

Thanks to the configuration possibility via the messenger, the application can adjust the scanner service to the appropriate process step. For process steps without scanner use, it can deactivate the automatic triggering of the scanner and use the scanner keys for other purposes.[^1]

The configuration of the permitted bar code types is helpful if the process steps use different bar code types and it can prevent mistaken scans.

### Using the Messenger for the M2Longrange Module
As of version 1.40 of the ACD ScanService app, it provides a second service for connecting the M2Longrange module to the M2Smart®SE. The name of the service is `de.acdgruppe.scanservicelr.AcdScannerServiceLR`. All the information in the previous paragraph applies equally.

### Examples of Connecting
**Hint: a full communication example can be found as a separate file in this project**

```java
Intent scanServiceIntent = new Intent();
ComponentName scanServiceName =
new ComponentName("de.acdgruppe.scanservice", "de.acdgruppe.scanservice.AcdScannerService");

scanServiceIntent.setComponent(scanServiceName);
if (getApplicationContext().bindService(scanServiceIntent,
scannerConnection, Context.BIND_AUTO_CREATE)) { .... }
```

```java
Intent scanServiceM2LRIntent = new Intent();
ComponentName scanServiceM2LRName =
new ComponentName("de.acdgruppe.scanservice", "de.acdgruppe.scanservicelr.AcdScannerServiceLR");

scanServiceM2LRIntent.setComponent(scanServiceM2LRName);
if (getApplicationContext().bindService(scanServiceM2LRIntent,
scannerM2LRConnection, Context.BIND_AUTO_CREATE)) { .... }
```

## The API of the service
It is assumed that you are familiar with the concept of binding to the service, so that will be sketched only briefly here.

1. The app calls bindService() in the appropriate place.
1. In the event onServiceConnected() the app can create a messenger object that it uses to send messages to the service. The app configures the service as it needs to.
1. Another messenger receives incoming messages from the service. The app sends bar codes that are received via the messenger to the associated program code.
1. In an appropriate place, the app calls unbindService() and releases the service again.

With use of several activities in the app, it is helpful to keep the service bound globally, that is, for the entire lifetime of the app, instead of binding it separately to each activity.

The service is controlled by messages. A message object includes the three numeric fields what, arg1 and arg2. what contains the message type, arg1 and arg2 contain possible other details about the message.


|**Message**|**Description**|**Message No (what-msg)**|
| --- | --- | --- |
|**OK**|Sent by the service as response to a request from the client app|1|
|**HELLO**|The service responds to HELLO with OK. Beyond that there is no login.|2|
|**CONFIGURE**|Message makes settings to the service|3|
|**SCAN**|Turn scan beam on and off from the application|4|
|**SET_TARGET**|Selection of barcode transfer|5|
|**BARCODE**|Message contains barcode information|6|
|**KEY_ENABLE**|Trigger scan when the scanner keys are pressed|7|
|**AIM**|Turn aiming beam on or off|8|

*Table 1: API ScanService*

### Message types
For what, there are the following possibilities:

#### OK
Sent by the service as a response to a request from the client app.


|**what**|**arg1**|**arg2**|
| --- | --- | --- |
|**1**|Return of the requested what<br/>Example: ‘2‘ for HELLO|0 = Not able to process<br/>1 = Response is OK|


#### HELLO
The service responds to HELLO with OK. The answer to HELLO contains data about the scanner. 
No login or other information exchange takes place.


|**what**|**arg1**|**arg2**|
| --- | --- | --- |
|**2**|No function|No function|

The OK message as response to HELLO contains information about the scanner in the bundle field of the message. 

The following entries are present:

- ENGINECODE: The code of the scanengine.
- SWREVISION: The software revision of the scanner.
- NAME: Name of the scanner (\*).
- MODULENR: The number of the scan module (\*).

The fields marked with (\*) are determined from the enginecode and are empty if a scanner unknown to the scanner service is installed.


#### CONFIGURE
This message makes settings for the service. Currently this has no function yet.


|**what**|**arg1**|**arg2**|
| --- | --- | --- |
|**3**|No function|No function|

#### **SCAN**
The client can trigger the scan stream or end it with this message. Note that the scanner switches off automatically after a certain time if there is no successful scan.

|**what**|**arg1**|**arg2**|
| --- | --- | --- |
|**4**|0 = End scan beam|No function|
|**4**|1 = Trigger scan beam|No function|
####
#### **SET\_TARGET**
The client uses this message to select how it would like to receive a scanned bar code. The options of the message will be explained in the next section.


|**what**|**arg1**|**arg2**|
| --- | --- | --- |
|**5**|Destination|No function|

The message SET\_TARGET controls the behavior of the service if it has successfully decoded a bar code. The last setting made applies regardless of the currently active app. Depending on the area of application and the operator's preference, the app should reset the service to the desired default setting at least before unbinding (unbind), for example the keyboard looping.

The field arg1 can take on the following values for the message SET\_TARGET:
- NONE
The services does not forward a scanned bar code. This value exists for the sake of completeness. A process step without a scan procedure should deactivate the scanner keys.
***arg1 = 0***

- INJECT
The service loops the bar code into the keyboard system. This is the default setting for the service. After the app terminates (or a pause), this line should be restored. See: "2.1.1 Tastatureinschleifung"
***arg1 = 1***

- BROADCAST\_INTENT
The bar code is forwarded via an intent sent by broadcast. See: „2.1.2 Versand eines Broadcast“
***arg1 = 2***

- MESSENGER
    The service sends the bar code to the messenger object that has specified this target. The sent bundle contains the fields:
    - "barcode" of type String: Contains the barcode
    - "type" of type String: Contains the barcode type
    - "typecode" of type int: Contains the type code

    See: „2.1.3 Verwendung des Messengers“ 
***arg1 = 3***

#### BARCODE
By means of this message, the service sends a scanned bar code to the client, provided the client has set this send type. The bar code is sent to the messenger object by which it selected the send type. It is only sent to the last messenger object to be set.


|**what**|**Return value**|
| --- | --- |
|**6**|Contains bundle with barcode information|

#### KEY\_ENABLE
This way, the client specifies whether or not the service triggers the operation of the scanner automatically with operation of the scanner keys.


|**what**|**arg1**|**arg2**|
| --- | --- | --- |
|**7**|0 = Do not trigger scan|No function|
|**7**|1 = Trigger|No function|


#### AIM
The client determines whether or not the aiming beam of the scanner will be triggered. 

This function is available with the 2D longrange scanner.


|**what**|**arg1**|**arg2**|
| --- | --- | --- |
|**8**|0 = Aiming beam off|No function|
|**8**|1 = Aiming beam on|No function|

### Barcode types
The ScanService returns a type code and the barcode type with every scanned barcode.


|**Barcode type**|**Typecode**|
| --- | --- |
|Code 39|01|
|Code 39 Full ASCII|13|
|Trioptic Code 39|15|
|Code 32|20|
|Code 93|07|
|Codabar|02|
|NW 7|18|
|Code 128|03|
|ISBT 128|19|
|ISBT 128 Concatenation|21|
|Discrete 2 of 5|04|
|IATA 2 of 5|05|
|Interleaved 2 of 5|06|
|Matrix 2 of 5|71|
|Chinese 2 of 5|72|
|Korean 3 of 5|73|
|UPC A|08|
|UPC A with 2 Supplementals|48|
|UPC A with 5 Supplementals|88|
|UPC E|09|
|UPC E with 2 Supplementals|49|
|UPC E with 5 Supplementals|89|
|UPC E1|10|
|4State US|34|
|4State US4|35|
|Scanlet Webcode|37|
|Cue CAT Code|38|
|French Lottery|2F|
|GS1 128|0F|
|GS1 DataBar 14|30|
|GS1 DataBar Limited|31|
|GS1 DataBar Expanded|32|
|GS1 DataBar Expanded Coupon|B4|
|GS1 Data Matrix|C1|
|PDF 417|11|
|Macro PDF 417|28|
|Micro PDF|1A|
|Micro PDF CCA|1D|
|Macro Micro PDF|9A|
|Data Matrix|1B|
|GS1 Data Matrix|C1|
|QR Code|1C|
|Micro QR Code|2C|
|Macro QR Code|29|
|Maxicode|25|
|Aztec|2D|
|UPC E1 with 2 Supplementals|50|
|UPC E1 with 5 Supplementals|90|
|EAN 8|0A|
|EAN 8 with 2 Supplementals|4A|
|EAN 8 with 5 Supplementals|8A|
|EAN 13|0B|
|EAN 13 with 2 Supplementals|4B|
|EAN 13 with 5 Supplementals|8B|
|Bookland EAN|16|
|Code 11|0C|
|Code 49|0D|
|MSI|0E|
|Code 16K|12|
|UPCD|14|
|Coupon Code|17|
|Postnet US|1E|
|Planet US|1F|
|Postal Japan|22|
|Postal Australia|23|
|Postal Dutch|24|
|Postbar CA|26|
|Postal UK|27|
|Aztec Rune Code|2E|
|Han Xin|B7|
|Signature|69|
|OCRB|A0|
|TLC 39|5A|
|Composite CC-A GS1-128|51|
|Composite CC-A EAN-13|52|
|Composite CC-A EAN-8|53|
|Composite CC-A GS1-DB Expanded|54|
|Composite CC-A GS1-DB Limited|55|
|Composite CC-A GS1-DataBar 14|56|
|Composite CC-A UPC-A|57|
|Composite CC-A UPC-E|58|
|Composite CC-C GS1-128|59|
|Composite CC-B GS1-128|61|
|Composite CC-B EAN-13|62|
|Composite CC-B EAN-8|63|
|Composite CC-B GS1-DB Expanded|64|
|Composite CC-B GS1-DB Limited|65|
|Composite CC-B GS1-DataBar 14|66|
|Composite CC-B UPC-A|67|
|Composite CC-B UPC-E|68|

*Table 2: Typecode Scanner without 2D-LR*

If a 2D Longrangescanner is installed, other type codes are reported as well.


|**Barcode type**|**Typecode**|
| --- | --- |
|Code 39|105|
|Codabar|101|
|Codablock|102|
|Australian Post|97|
|Canada Post|100|
|Dutch Post|110|
|Japan Post|120|
|Sweden Post|73|
|Aztec|98|
|BP0|99|
|Code11|104|
|Code 128/GS1-128|107|
|DataMatrix|109|
|EAN/UPC|111|
|Infomail|118|
|Matrix 2 of 5|121|
|MaxiCode|122|
|PDF417/MicroPDF417|66|
|Planet|68|
|Plessey Code|69|
|Postnet|70|
|QR Code|71|
|Standard 2 of 5|72|
|Telepen|74|
|TLC39|75|
|Interleaved 2 of 5|119|
|Code 93|106|
|MSI Code|65|
|GS1 DataBar|117|
|GS1 Composite|115|

*Table 3: Typecode Scanner 2D-LR*


## Scanner configuration
The ACD ScanConfig app is pre-installed on the ACD devices and will make scanner settings for this. Among other things, the app can be used to make the following settings:

- Configuration of scanner parameters (permitted bar codes, etc.)
- Configuration of the scanner data (attachment of prefix, suffix, etc.)
- Notification tone when scanning a bar code
- Configuration of the scanner keys
- Etc.

The configuration data of the ACD ScanConfig will be stored on the device under */sdcard/ACD/scanner*.

The ACD ScanService uses the settings that are made in the ACD ScanConfig in order to configure the scanner installed in the device.


[^1]: The keys are reported to the activity by the usual events (onKeyDown etc.), regardless of whether or not the scan service triggers the scanner.
