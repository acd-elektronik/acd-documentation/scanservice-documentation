# ACD ScanService documentation and examples 

## Content of this Project

### Documentation

[ACD ScanService documentation](documentation.md)

### Examples

[ACD ScanService connection example](ConnectionExample.java)  