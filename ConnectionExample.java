public class MainActivity extends AppCompatActivity {

    private Messenger _serviceMessenger = null;
    private boolean _boundToService = false;
    private TextView txtbarcode;

    public static final int WHAT_OK = 1;
    public static final int WHAT_HELLO = 2;
    public static final int WHAT_SCAN = 4;
    public static final int WHAT_SET_TARGET = 5;
    public static final int WHAT_BARCODE = 6;
    public static final int WHAT_KEY_ENABLE = 7;
    public static final int TARGET_MESSAGE = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtbarcode = findViewById(R.id.txtbarcode);
        bindToService(this);
    }

    public void bindToService(Context context) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("de.acdgruppe.scanservice", "de.acdgruppe.scanservice.AcdScannerService"));
        // for Longrange use: new ComponentName("de.acdgruppe.scanservicelr", "de.acdgruppe.scanservicelr.AcdScannerServiceLR")

        try {
            // Use applicationContext because the activity can change during rotation and it is not
            // obvious what happens to the service if bound to the activity.
            if (!context.bindService(intent, _connection, Context.BIND_AUTO_CREATE))
                return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ServiceConnection _connection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been established, giving us the
            // service object we can use to interact with the service.
            _serviceMessenger = new Messenger(service);

            // Send a hello to the service. Next steps follow when the response arrives.
            _boundToService = true;
            sendMessage(WHAT_HELLO, 0, 0);
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected, its process crashed.
            _serviceMessenger = null;
            _boundToService = false;
        }
    };

    public void sendMessage(int what, int arg1, int arg2) {
        if (_serviceMessenger == null) {
            // The messenger should never be null, there's one not-changing instance during the app's life.
            String text = String.format("Service messenger is null! (Message=%d)", what);
            return;
        }

        if (!_boundToService) {

            return;
        }

        try {
            Message msg = Message.obtain(null, what, arg1, arg2);
            msg.replyTo = _receiverMessenger;
            _serviceMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private Messenger _receiverMessenger = new Messenger(new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case WHAT_OK:
                    // arg1 contains the reason for this OK message
                    switch (message.arg1) {

                        case WHAT_HELLO:
                            // We are connected => set the service to send barcode messages via messenger
                            sendMessage(WHAT_SET_TARGET, TARGET_MESSAGE, 0);
                            // Enable the scankey trigger
                            sendMessage(WHAT_KEY_ENABLE, 1, 0);
                            break;
                    }
                    break;

                case WHAT_BARCODE:
                    txtbarcode.setText(message.getData().getString("barcode"));
                    break;
            }
            return true;
        }
    }));
}